<?php 

if(isset($_GET['function'])) if(function_exists($_GET['function'])) call_user_func($_GET['function']);

$origin = (isset($_GET['origin']) ? $_GET['origin'] : NULL );
$depart_time = (isset($_GET['depart_time']) ? $_GET['depart_time'] : NULL );
$dest = (isset($_GET['dest']) ? $_GET['dest'] : NULL );
$arrive_time = (isset($_GET['arrive_time']) ? $_GET['arrive_time'] : NULL );

if($origin || $depart_time || $dest || $arrive_time) search($origin,$depart_time,$dest,$arrive_time);

function get_data() {
	
	global $download_usr, $download_pass, $data_file_path, $download_url;
	
	set_time_limit(0);
	$fp = fopen ($data_file_path.'.gz', 'w+');//This is the file where we save the    information
	$ch = curl_init($download_url);//Here is the file we are downloading, replace spaces with %20
	
	curl_setopt($ch, CURLOPT_TIMEOUT, 500);
	curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
	curl_setopt($ch, CURLOPT_USERPWD, $download_usr.':'.$download_pass);
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	
	curl_exec($ch); // get curl response
	
	curl_close($ch);
	
	fclose($fp);

}

function uncompress($srcName, $dstName) {
	
	global $data_file_path;
	
	$srcName = ($srcName ? $srcName : $data_file_path.'.gz');
	$dstName = ($dstName ? $dstName : $data_file_path.'.data');
	
    $sfp = gzopen($srcName, "rb");
    $fp = fopen($dstName, "w");

    while ($string = gzread($sfp, 4096)) {
        fwrite($fp, $string, strlen($string));
    }
    gzclose($sfp);
    fclose($fp);
}

function db_connect() {
	
	global $db_path, $db_name, $db_user, $db_pass;
	
	$con = new MySQLi($db_path, $db_user, $db_pass, $db_name);
	
	if ($con->connect_errno) {
		return $mysqli->connect_errno;	
	} else {
		return $con;
	}
	
}

function do_import($record) {
	
	if(!isset($record)) return false;
	
	$con = db_connect();
	
	if(!$con) return 'Error no: '.$con;
	
	$query = "INSERT INTO train_data (
						CIF_train_uid,
						CIF_bank_holiday_running,
						CIF_stp_indicator,
						applicable_timetable,
						atoc_code
					)
					VALUES
					(
						'$record[CIF_train_uid]',
						'$record[CIF_bank_holiday_running]',
						'$record[CIF_stp_indicator]',
						'$record[applicable_timetable]',
						'$record[atoc_code]'
					)";
	
	if (!mysqli_query($con,$query)) {
		return 'Error: ' . mysqli_error($con);
	}
	
	$schedule = $record['schedule'];
	
	$query = "INSERT INTO schedule_data (
						train_id, 
						schedule_days_runs, 
						schedule_start_date, 
						schedule_end_date, 
						signalling_id, 
						CIF_train_category, 
						CIF_headcode, 
						CIF_course_indicator, 
						CIF_train_service_code, 
						CIF_business_sector, 
						CIF_power_type, 
						CIF_timing_load, 
						CIF_speed, 
						CIF_operating_characteristics, 
						CIF_train_class, 
						CIF_sleepers, 
						CIF_reservations, 
						CIF_connection_indicator, 
						CIF_catering_code, 
						CIF_service_branding	
					)
					SELECT
						train_data.id,					
						'$schedule[schedule_days_runs]',
						'$schedule[schedule_start_date]',
						'$schedule[schedule_end_date]',
						'$schedule[signalling_id]',
						'$schedule[CIF_train_category]',
						'$schedule[CIF_headcode]',
						'$schedule[CIF_course_indicator]',
						'$schedule[CIF_train_service_code]',
						'$schedule[CIF_business_sector]',
						'$schedule[CIF_power_type]',
						'$schedule[CIF_timing_load]',
						'$schedule[CIF_speed]',
						'$schedule[CIF_operating_characteristics]',
						'$schedule[CIF_train_class]',
						'$schedule[CIF_sleepers]',
						'$schedule[CIF_reservations]',
						'$schedule[CIF_connection_indicator]',
						'$schedule[CIF_catering_code]',
						'$schedule[CIF_service_branding]'
					FROM train_data WHERE CIF_train_uid = '$record[CIF_train_uid]'";
					
	if (!mysqli_query($con,$query)) {
		return 'Error: ' . mysqli_error($con);
	}
	
	$locations = $schedule['locations'];
	
	if($locations) foreach($locations as $location){
		
		$query = "INSERT INTO schedule (
						schedule_id,
						train_id, 
						location_id, 
						record_identity, 
						arrival, 
						departure, 
						pass, 
						public_arrival, 
						public_departure, 
						platform, 
						line, 
						path, 
						engineering_allowance, 
						pathing_allowance, 
						performance_allowance, 
						location_type
					)
					SELECT
						schedule_data.id,					
						train_data.id,
						location_data.id,
						'$location[record_identity]',
						'$location[arrival]',
						'$location[departure]',
						'$location[pass]',
						'$location[public_arrival]',
						'$location[public_departure]',
						'$location[platform]',
						'$location[line]',
						'$location[path]',
						'$location[engineering_allowance]',
						'$location[pathing_allowance]',
						'$location[performance_allowance]',
						'$location[location_type]'
					FROM schedule_data
					INNER JOIN train_data
						ON schedule_data.train_id=train_data.id
					JOIN location_data
					WHERE train_data.CIF_train_uid = '$record[CIF_train_uid]'
						AND location_data.tiploc = '$location[tiploc_code]'";
						
		if (!mysqli_query($con,$query)) {
			return 'Error: ' . mysqli_error($con);
		}
		
	}
					
	return true;
	
}

function put_data(){
	$latest_ctime = 0;
	$latest_filename = '';
	
	global $path;
	
	$directory = dir($path);
		
	while (false !== ($entry = $directory->read())) {
		$filepath = "{$path}".DIRECTORY_SEPARATOR."{$entry}";
		// could do also other checks than just checking whether the entry is a file
		if (is_file($filepath) && filectime($filepath) > $latest_ctime && !strpos('.gz',$entry)) {
			$latest_ctime = filectime($filepath);
			$latest_filename = $entry;
		}
	}
	
	$file=fopen($path.'/'.$latest_filename,READ_ONLY) or exit ('Unlucky!');
	
	$i=0;
	
	while(!feof($file)) {
		$line = json_decode(fgets($file));
		if(property_exists($line,'JsonScheduleV1')) {
			$schedule = $line->JsonScheduleV1;
			
			$record['CIF_train_uid'] = ($schedule->CIF_train_uid ? $schedule->CIF_train_uid : NULL);
			$record['CIF_bank_holiday_running'] = ($schedule->CIF_bank_holiday_running ? $schedule->CIF_bank_holiday_running : NULL);
			$record['CIF_stp_indicator'] = ($schedule->CIF_stp_indicator ? $schedule->CIF_stp_indicator : NULL);
			$record['applicable_timetable'] = ($schedule->applicable_timetable ? $schedule->applicable_timetable : NULL);
			$record['atoc_code'] = ($schedule->atoc_code ? $schedule->atoc_code : NULL);
			
			$i++;
			if(property_exists($schedule,'schedule_segment')) {
				$schedule_segment = $schedule->schedule_segment;
				
				$record['schedule']['schedule_days_runs'] = ($schedule->schedule_days_runs ? $schedule->schedule_days_runs : NULL);
				$record['schedule']['schedule_start_date'] = ($schedule->schedule_start_date ? $schedule->schedule_start_date : NULL);
				$record['schedule']['schedule_end_date'] = ($schedule->schedule_end_date ? $schedule->schedule_end_date : NULL);
				$record['schedule']['signalling_id'] = ($schedule_segment->signalling_id ? $schedule_segment->signalling_id : NULL);
				$record['schedule']['CIF_train_category'] = ($schedule_segment->CIF_train_category ? $schedule_segment->CIF_train_category : NULL);
				$record['schedule']['CIF_headcode'] = ($schedule_segment->CIF_headcode ? $schedule_segment->CIF_headcode : NULL);
				$record['schedule']['CIF_course_indicator'] = ($schedule_segment->CIF_course_indicator ? $schedule_segment->CIF_course_indicator : NULL);
				$record['schedule']['CIF_train_service_code'] = ($schedule_segment->CIF_train_service_code ? $schedule_segment->CIF_train_service_code : NULL);
				$record['schedule']['CIF_business_sector'] = ($schedule_segment->CIF_business_sector ? $schedule_segment->CIF_business_sector : NULL);
				$record['schedule']['CIF_power_type'] = ($schedule_segment->CIF_power_type ? $schedule_segment->CIF_power_type : NULL);
				$record['schedule']['CIF_timing_load'] = ($schedule_segment->CIF_timing_load ? $schedule_segment->CIF_timing_load : NULL);
				$record['schedule']['CIF_speed'] = ($schedule_segment->CIF_speed ? $schedule_segment->CIF_speed : NULL);
				$record['schedule']['CIF_operating_characteristics'] = ($schedule_segment->CIF_operating_characteristics ? $schedule_segment->CIF_operating_characteristics : NULL);
				$record['schedule']['CIF_train_class'] = ($schedule_segment->CIF_train_class ? $schedule_segment->CIF_train_class : NULL);
				$record['schedule']['CIF_sleepers'] = ($schedule_segment->CIF_sleepers ? $schedule_segment->CIF_sleepers : NULL);
				$record['schedule']['CIF_reservations'] = ($schedule_segment->CIF_reservations ? $schedule_segment->CIF_reservations : NULL);
				$record['schedule']['CIF_connection_indicator'] = ($schedule_segment->CIF_connection_indicator ? $schedule_segment->CIF_connection_indicator : NULL);
				$record['schedule']['CIF_catering_code'] = ($schedule_segment->CIF_catering_code ? $schedule_segment->CIF_catering_code : NULL);
				$record['schedule']['CIF_service_branding'] = ($schedule_segment->CIF_service_branding ? $schedule_segment->CIF_service_branding : NULL);	
				
				if(property_exists($schedule_segment,'schedule_location')) {
					$locations = $schedule_segment->schedule_location;
					
					$n = 0;
					foreach($locations as $location){
						$record['schedule']['locations'][$n]['record_identity'] = ($location->record_identity ? $location->record_identity : NULL);
						$record['schedule']['locations'][$n]['arrival'] = ($location->arrival ? $location->arrival : NULL);
						$record['schedule']['locations'][$n]['departure'] = ($location->departure ? $location->departure : NULL);
						$record['schedule']['locations'][$n]['pass'] = ($location->pass ? $location->pass : NULL);
						$record['schedule']['locations'][$n]['public_arrival'] = ($location->public_arrival ? $location->public_arrival : NULL);
						$record['schedule']['locations'][$n]['public_departure'] = ($location->public_departure ? $location->public_departure : NULL);
						$record['schedule']['locations'][$n]['platform'] = ($location->platform ? $location->platform : NULL);
						$record['schedule']['locations'][$n]['line'] = ($location->line ? $location->line : NULL);
						$record['schedule']['locations'][$n]['path'] = ($location->path ? $location->path : NULL);
						$record['schedule']['locations'][$n]['engineering_allowance'] = ($location->engineering_allowance ? $location->engineering_allowance : NULL);
						$record['schedule']['locations'][$n]['pathing_allowance'] = ($location->pathing_allowance ? $location->pathing_allowance : NULL);
						$record['schedule']['locations'][$n]['performance_allowance'] = ($location->performance_allowance ? $location->performance_allowance : NULL);
						$record['schedule']['locations'][$n]['location_type'] = ($location->location_type ? $location->location_type : NULL);
						$record['schedule']['locations'][$n]['tiploc_code'] = ($location->tiploc_code ? $location->tiploc_code : NULL);
						$n++;
					}
				}
			}
			echo do_import($record);
			//print_r($record);
	
		}
	}
	fclose($file);
}

function search($origin,$depart_time,$dest,$arrive_time) {
	global $search_results;
	
	$search_results = 'Origin: '.$origin.', departing at '.$depart_time.'. Destination: '.$dest.' at '.$arrive_time.'.';
	
	$con = db_connect();
	
	if(!$con) {
		$search_results = 'Error no: '.$con;
		return;
	}
	
	$query = "SELECT * FROM schedule
				INNER JOIN location_data
				ON schedule.location_id=location_data.id
				WHERE location_data.crs = '$origin'
				OR location_data.location = '$origin'
				AND location_data.crs IS NOT NULL
				AND schedule.departure > '$depart_time'";
	
	if (!$origin = mysqli_query($con,$query)) {
		$search_results = 'Error: ' . mysqli_error($con);
		return;
	}
	
	$search_results = '';
	
	while($row = mysqli_fetch_array($origin)) {
		$train_ids[] = $row['train_id'];
		$origin_list[$row['train_id']] = $row;
	}
	
	if(!empty($train_ids)) {
		
		$train_id_string = "'".implode("', '",$train_ids)."'";
		
		$query = "SELECT * FROM schedule
				INNER JOIN location_data
				ON schedule.location_id=location_data.id
				WHERE location_data.crs = '$dest'
				OR location_data.location = '$dest'
				AND location_data.crs IS NOT NULL";
	
		if (!$results = mysqli_query($con,$query)) {
			$search_results = 'Error: ' . mysqli_error($con);
			return;
		}
		
		while($row = mysqli_fetch_array($results)) {
			if(array_key_exists($row['train_id'],$origin_list)){
				if($origin_list[$row['train_id']]['departure'] < $row['arrival']){
					$search_results .= sprintf('<p>%1$s [%2$s] to %3$s [%4$s] departing at %5$s and arriving at %6$s.</p>',
											$origin_list[$row['train_id']]['location'],
											$origin_list[$row['train_id']]['crs'],
											$row['location'],
											$row['crs'],
											$origin_list[$row['train_id']]['departure'],
											$row['arrival']);
				}
			}
		}
	}
	$search_results = ($search_results ? $search_results : 'Sorry, nothing found... try broadening your search.');
}

function get_locations($term) {
	
	$con = db_connect();
	$locations = array();
	
	if(!$con) {
		$search_results = 'Error no: '.$con;
		return;
	}
	
	$query = "SELECT * FROM location_data WHERE location LIKE '%$term%' OR crs LIKE '%$term%'";
	
	if (!$results = mysqli_query($con,$query)) {
		$search_results = 'Error: ' . mysqli_error($con);
		return;
	}
	
	while($row = mysqli_fetch_array($results)) {
		if($row['location'] && $row['crs']){
			$locations[] = array(
					'label' => sprintf('%1$s [%2$s]', $row['location'], $row['crs']),
					'value' => $row['location'],
			);
		}
	}
	
	return json_encode($locations);
}

?>