<?php

include('config.php');
include('functions.php');

global $search_results;

include('header.php');

?>

<div id="content">
<form action="">
	<div class="row">
        <label title="origin" for="origin">From:</label>
        <input id="origin" type="text" name="origin" title="origin" placeholder="Station of origin" value="<?php echo $origin; ?>"/>
	</div>
    <div class="row">
        <label title="depart_time" for="depart_time">Time:</label>
        <input type="text" name="depart_time" title="depart_time" placeholder="Like 1145" value="<?php echo $depart_time; ?>"/>
	</div>
    <div class="row">
        <label title="dest" for="dest">To:</label>
        <input id="dest" type="text" name="dest" title="dest" placeholder="Destination" value="<?php echo $dest; ?>"/>
	</div>
    <div class="row">
        <label title="arrive_time" for="arrive_time">Time:</label>
        <input type="text" name="arrive_time" title="arrive_time" placeholder="Like 1545" value="<?php echo $arrive_time; ?>"/>
	</div>
    <div class="row">
    	<input type="submit" value="Go!" />
    </div>
    <div class="row">
    	<input id="switch" type="button" value="Switch" />
    </div>
</form>
<?php
if($search_results) echo $search_results;
?>
</div>

<?php include('footer.php'); ?>
