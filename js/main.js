// JavaScript Document

jQuery(document).ready(function($){
	$('#origin, #dest').autocomplete({source:'ajax.php?request=1', minLength:2, delay:0});
	
	$('#switch').click(function(){
		var origin = $('#origin').val();
		var dest = $('#dest').val();
		$('#origin').val(dest);
		$('#dest').val(origin);
		$('#content form').submit();
	});
	
});