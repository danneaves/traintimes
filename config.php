<?php

// **********************************************************
//
// App configuration variables set here
//
// **********************************************************

$db_path = '217.199.187.64';
$db_name = 'cl51-trains';
$db_user = 'cl51-trains';
$db_pass = 'dFrEsXcVgT5$3@1';

const READ_ONLY = 'r',
	READ_WRITE = 'r+',
	WRITE = 'w',
	WRITE_READ = 'w+',
	APPEND = 'a',
	READ_APPEND = 'a+',
	WRITE_X = 'x',
	READ_WRITE_X = 'x+';

$data_path = '_data';

$full_download_type = 'CIF_ALL_FULL_DAILY';
$full_download_day = 'toc-full';

$update_download_type = 'CIF_ALL_UPDATE_DAILY';
$update_download_day = 'toc-update-'.strtolower(date('D',time() - (1 * 24 * 60 * 60)));

$default_data_file = 'default';

$download_usr = 'dan@danielneaves.com';
$download_pass = 'dFrEsXcVgT5$3@1';
$download_url = 'https://datafeeds.networkrail.co.uk/ntrod/CifFileAuthenticate?type=CIF_ALL_FULL_DAILY&day=toc-full';
$full_download_url = 'https://datafeeds.networkrail.co.uk/ntrod/CifFileAuthenticate?type='.$full_download_type.'&day='.$full_download_day;
$update_download_url = 'https://datafeeds.networkrail.co.uk/ntrod/CifFileAuthenticate?type='.$update_download_type.'&day='.$update_download_day;

$path = getcwd().DIRECTORY_SEPARATOR.$data_path;
$data_file_path = $path.DIRECTORY_SEPARATOR.$default_data_file;

?>